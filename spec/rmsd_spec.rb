require 'spec_helper'
require 'ruby-prof'


describe Rmsd do
  it 'has a version number' do
    expect(Rmsd::VERSION).not_to be nil
  end


  describe 'Struct::Atom' do

    before :all do
      @zero = NMatrix.new [1,3], [0,0,0], dtype: :float64
    end

    it 'takes coords in NMatrix [1,3] float64 format' do
      a = Struct::Atom.new @zero
      expect(a ).to be_instance_of Struct::Atom
    end

    it 'takes optional type and name parameters' do
      a = Struct::Atom.new @zero, "Ln", "Lantan_Vasya"
      expect(a ).to be_instance_of Struct::Atom
    end

    it 'has :coords, :type and :name getters' do
      a = Struct::Atom.new @zero, "Ln", "Lantan_Vasya"
      expect(a.coords ).to eq @zero
      expect(a.type).to eq "Ln"
      expect(a.name).to eq "Lantan_Vasya"
    end

  end

  describe 'NMatrix' do
    before :all do
      @left  =  NMatrix.eye 3
      @right =  N[[0,1,0], [1,0,0], [0,0,1], dtype: :float64]
      @some_left = N[ [1,1,0],
                      [2,-1,0],
                      [-3,-1,0],
                      [-1,-1,0],
                      [0,0,2], dtype: :float64]
      @some_right = N[ [1,1,0],
                      [2,-1,0],
                      [-3,-1,0],
                      [-1,-1,0],
                      [0,0,-2], dtype: :float64]

      
      @zeros =  NMatrix.new [1,3], [0,0,0], dtype: :float64
      @ones  =  NMatrix.new [1,3], [1,1,1], dtype: :float64
      @twos  =  NMatrix.new [1,3], [2,2,2], dtype: :float64
    end

    describe 'NMatrix.vconcat' do
      it 'takes array of NMatrix and vconcatenate it' do
        a = NMatrix.vconcat [@zeros, @ones, @twos]
        b = N[[0,0,0],[1,1,1],[2,2,2], dtype: :float64]
        expect(a).to eq b      
      end

      it 'not fails when array size = 1' do
        a = NMatrix.vconcat [@zeros]
        b = N[[0,0,0], dtype: :float64]
        expect(a).to eq b      
      end

      it 'not fails with empty array' do
        expect(NMatrix.vconcat []).to eq []
      end
    end

    describe 'NMatrix#square_sum' do
      it 'returns square sum of NMatrix elements' do
        expect(@left.square_sum).to eq 3.0
        expect(@zeros.square_sum).to eq 0.0
        expect(@twos.square_sum).to eq 12.0
      end
    end


    describe 'NMatrix#kabsch' do

      it 'takes NMatrix and returns hash' do
        expect(@ones.kabsch(@twos)).to be_instance_of Hash
      end
      
      it 'returns rmsd of the best overlay as a value at :rmsd key' do
        expect(@ones.kabsch(@twos)[:rmsd] ).to be_within(0.0001).of Math.sqrt 3
      end

      it 'respects chirality by default' do
        expect(@left.kabsch(@right)[:rmsd]).to be > 0.0001
        expect(@some_left.kabsch(@some_right)[:rmsd]).to be > 0.0001
      end

      it 'ignores chirality with allow_improper:true' do
        expect(@left.kabsch(@right, allow_improper:true)[:rmsd]).to be < 0.0001
        expect(@some_left.kabsch(@some_right, allow_improper:true)[:rmsd]).to be < 0.0001
        expect(@ones.kabsch(@twos, allow_improper:true)[:rmsd] ).to be_within(0.0001).of Math.sqrt 3
      end

      it 'returns rotation matrix of the best at :rotation key with rotation:true' do
        a = @right.kabsch(@right, rotation:true)
        expect(a[:rotation]).to eq @left
      end
      
    end
    
  end

  describe 'Molecule' do
    before :all do
      @mol1     =  Molecule.new (0..9).to_a.map{|i| Struct::Atom.new N[[i,i,i]],  "La", "Lantan_Vasya_#{i+1}" }
      @mol2     =  Molecule.new (1..10).to_a.map{|i| Struct::Atom.new N[[i,i,i]],  "La", "Lantan_Vasya_#{i}" }
    end

    it 'has working == method' do
      expect(@mol1).to eq  Molecule.new (0..9).to_a.map{|i| Struct::Atom.new N[[i,i,i]],  "La", "Lantan_Vasya_#{i+1}" }
      expect(@mol1).not_to eq @mol2
    end
      
    
    it 'calculates rmsd' do
      expect(@mol1.rmsd @mol2).to eq Math.sqrt 3
      expect(@mol1.rmsd @mol2).to eq Math.sqrt 3
    end

    it 'translates' do
      zero = N[[0, 0, 0], dtype: :float64]
      ones = N[[1, 1, 1], dtype: :float64]
      expect(@mol1.translate(zero)).to eq @mol1
      expect(@mol1.translate(ones)).to eq @mol2
    end

    it 'deletes atom' do
      expect( @mol2.delete_atom ).to eq Molecule.new (2..10).to_a.map{|i| Struct::Atom.new N[[i,i,i]],  "La", "Lantan_Vasya_#{i}" }
      expect(@mol2.delete_atom num=9).to eq Molecule.new (1..9).to_a.map{|i| Struct::Atom.new N[[i,i,i]],  "La", "Lantan_Vasya_#{i}" }
    end
    
    it 'polyhedrifies' do
      mol1_polyhedra = Molecule.new (1..9).to_a.map{|i| Struct::Atom.new N[[i,i,i]], "La", "Lantan_Vasya_#{i+1}" } 
      expect(@mol1.polyhedrify).to eq mol1_polyhedra
    end

    it 'calculates best_rmsd' do
      ## implement it!
      expect(true).to be false
    end
    
  end



  shared_examples 'OrderSearch' do
    let(:order_search) {described_class.new()}
    
    it 'has run_search(mol1, mol2) method' do
      expect(order_search).to respond_to(:run_search).with(2).arguments
    end
  end
    
  describe FixedOrder do
    it_behaves_like 'OrderSearch'   
  end
    
  

  describe BruteForceOrder do
    it_behaves_like 'OrderSearch'
  end
    
  

  describe 'CoordParser' do
    before :all do
      @pa = CoordParser.new
    end

    it 'parses na6_small.cor to array' do
      a = @pa.parse_with_debug IO.read 'spec/na6_small.cor'
      expect(a).to be_a Array
      expect(a.size).to eq 2
    end

    it 'parses na7_small.cor to array' do
      a = @pa.parse_with_debug IO.read 'spec/na7_small.cor'
      expect(a).to be_a Array
      expect(a.size).to eq 5
    end

    it 'transormes to array of moleculas with CoordTrans' do
      transformer = CoordTrans.new
      a = @pa.parse_with_debug IO.read 'spec/na6_small.cor'
      b = transformer.apply a
      expect(b).to be_a Array
      expect(b[0]).to be_a Molecule
    end
  end

end
