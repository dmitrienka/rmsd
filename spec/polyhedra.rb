module Polyhedra

  axaqul_atoms = [
    Atom.new(  1.85281, -0.12311, -1.51818, "O",    "O8" ), 
    Atom.new( -1.83872, -0.50100,  1.35177, "O",    "O2" ), 
    Atom.new( -1.50020,  1.77669, -0.32237, "O",    "O5" ), 
    Atom.new( -0.14300,  1.46610, -1.87592, "O",    "O6" ), 
    Atom.new( -0.15109, -1.57555,  1.81402, "O",    "O3" ), 
    Atom.new(  1.52724,  1.68074,  0.61620, "O",    "O12"), 
    Atom.new(  0.27375,  1.02456,  2.06122, "O",    "O11"), 
    Atom.new( -0.30043, -2.11690, -0.98760, "O",    "O15"), 
    Atom.new( -1.68981, -0.72314, -1.61465, "O",    "O14"), 
    Atom.new(  2.07201, -1.06407,  0.32880, "O",    "O9" )
  ]

  AXAQUL = Molecule.new axaqul_atoms


  hedtal_atoms = [
    Atom.new( 1.88264, -0.90616,  1.30497, 'O',    'O44'),
    Atom.new(-0.36285, -2.36358,  0.27116, 'O',    'O37'),
    Atom.new( 0.57110,  1.62352, -1.66571, 'O',    'O41'),
    Atom.new( 2.04153, -0.97204, -0.77475, 'O',    'O43'),
    Atom.new( 1.48589,  1.86139,  0.17674, 'O',    'O40'),
    Atom.new(-0.33630, -1.02209, -2.13540, 'O',    'O50'),
    Atom.new(-1.97721, -0.04318, -1.33644, 'O',    'O49'),
    Atom.new(-1.43670,  1.62833,  0.81833, 'O',    'O47'),
    Atom.new(-1.62850, -1.25198,  1.17907, 'O',    'O38'),
    Atom.new(-0.15786,  0.88322,  2.22741, 'O',    'O46')
  ]

  HEDTAL = Molecule.new hedtal_atoms

  pa = CoordParser.new
  tr = CoordTrans.new
  
  NA6_2 = (tr.apply pa.parse IO.read 'spec/na6_small.cor' ).map{|mol| mol.polyhedrify }
  NA6_4 = (tr.apply pa.parse IO.read 'spec/na6_small2.cor').map{|mol| mol.polyhedrify }
  NA5   = (tr.apply pa.parse IO.read 'spec/na5.cor'       ).map{|mol| mol.polyhedrify }
  NA6   = (tr.apply pa.parse IO.read 'spec/na6.cor'       ).map{|mol| mol.polyhedrify }
  NA7   = (tr.apply pa.parse IO.read 'spec/na7.cor'       ).map{|mol| mol.polyhedrify }  
  
end
