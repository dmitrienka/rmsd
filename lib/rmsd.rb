require_relative "rmsd/version"

module Rmsd
  require_relative "rmsd/nmatrix_monkey"
  require_relative "rmsd/molecule"
  require_relative "rmsd/order_search"
    require_relative "rmsd/best_first_search"
  require_relative "rmsd/parslet_monkey"
  require_relative "rmsd/coords_parser"
  require_relative "rmsd/coords_transform.rb"
end
