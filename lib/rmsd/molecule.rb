require "nmatrix"
require "nmatrix/lapack_plugin"


Struct.new("Atom", :coords, :type, :name )

class Molecule

  def initialize atoms, name = nil
    @n      = atoms.size
    @coords = NMatrix.vconcat atoms.map{|atom| atom.coords }
    @types  = atoms.map{|atom| atom.type}
    @names  = atoms.map{|atom| atom.name}
    @name   = name
  end

  attr_accessor :atoms, :n, :coords,  :types, :names, :name

  def translate vector
    translation = NMatrix.vconcat Array.new(n, vector)
    result = self.clone
    result.coords = coords + translation
    result
  end

  def delete_atom num=0, newname = name
    result = Molecule.new []
    result.coords = NMatrix.vconcat coords.each_row.reject.with_index{|row, i| i == num }
    result.types = types.reject.with_index {|x, i| i == num}
    result.names = names.reject.with_index {|x, i| i == num}
    result.n = n-1
    result.name = name
    result
  end

  def polyhedrify num = 0
    vector = coords.each_row.to_a[num]
    delete_atom(num).translate(- vector)
  end

  
  def rmsd other
    Math.sqrt (coords - other.coords).square_sum / n
  end
    
  def best_rmsd other, method
    method.run_search self, other
  end

  def == other, params = {tolerance: 0.000001}
    other.class == self.class &&
      other.name == name &&
      other.types == types &&
      other.names == names &&
      rmsd(other) < params[:tolerance]
  end
    
end

