require 'parslet'
require 'parslet/convenience'

class CoordParser < Parslet::Parser

  root :file
  
  rule :file do
    fragment.repeat(1)
  end
  
  rule :fragment do
    nameline.as(:frag_name) >> space? >> eol >>
    (atomline.as(:atom)    >> space? >> eol).repeat(1).as(:atoms)
  end

  rule :nameline do
    (match['A-Z'].repeat(6,6) >> match['0-9'].repeat(2,2).maybe).as(:refcode) >>
      space? >> str('**FRAG**') >> space >> integer.as(:frag_num)
  end

  rule :atomline do
    (match['A-Z'] >> match['a-z'].maybe).as(:atom_type) >> word.maybe.as(:atom_number) >>
      space >> float.as(:x) >> space >> float.as(:y) >> space >> float.as(:z) >> space >> int
  end
  
  
end
