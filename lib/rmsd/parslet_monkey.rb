require 'parslet'

class Parslet::Parser
  ## Open f'cking class!
  
  rule(:space)      { match('[ ]').repeat(1) }
  rule(:space?)     { space.maybe }
  rule(:eol)        {line_end.repeat(1)}
  rule(:eol?)       {eol.maybe }
  rule(:line_end)   {crlf >> space.maybe}
  rule(:crlf)       {match('[\r\n]').repeat(1)}
  rule(:int)        { match('[0-9]').repeat(1)} #silent
  rule(:integer)    {int.as(:int)}

  rule :real do                                 #silent
    (str('-').maybe >> int >>
      str('.') >> int)
  end

  rule :float do                              
    real.as(:float)
  end

  rule :string do
    str('"') >> 
      (
       (str('\\') >> any) |
         (str('"').absent? >> any)
      ).repeat.as(:string) >> 
      str('"')
  end

  rule :word do
    (match['\s'].absent? >> any).repeat(1)
  end

end


class Parslet::Transform
  rule(:int   => simple(:n)) {Integer(n)}
  rule(:float => simple(:f)) {Float(f)}
end
