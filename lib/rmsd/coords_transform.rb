class CoordTrans < Parslet::Transform
  rule(:atom =>subtree(:a)) do
    coords = NMatrix.new([1,3], [a[:x], a[:y], a[:z]], dtype: :float64)
    type =  a[:atom_type]
    name = a[:atom_type] + a[:atom_number]
    Struct::Atom.new coords = coords, type =  type, name = name
  end

  rule(:refcode => simple(:ref), :frag_num => simple(:num)) do
    ref.to_s + '_' + num.to_s
  end

  rule(:frag_name => simple(:name), :atoms => sequence(:atoms_arr)) do
    Molecule.new atoms_arr, name
  end
  
end
