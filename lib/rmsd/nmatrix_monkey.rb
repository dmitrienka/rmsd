class NMatrix

  def self.vconcat ary
    return ary if ary.empty? 
    ary.shift.vconcat *ary
  end
  
  
  def square_sum
    self.inject(0){|memo, n| memo + n**2 }
  end

  
  def kabsch other, params = {}

    if ord = params[:order]
      ot = other.transpose.permute_columns(ord, convention: :intuitive)
    else
      ot = other.transpose
    end
    
    v, s, wt = ot.dot(self).gesvd
    vt =  v.transpose
    w  = wt.transpose

    if params[:allow_improper]
      d = 1
    else
      d = w.dot(vt).det <=> 0
    end

    result = {}
    
    if params[:rotation]
      dm = NMatrix.new([3,3], [1,0,0,0,1,0,0,0, d], type: :float64)
      result[:rotation] = w.dot(dm).dot(vt)
    end

    n = shape[0]
    
    e = (square_sum + other.square_sum)/n -
      (s[0] + s[1] + (s[2] * d)) *2/n

    result[:rmsd] = (e > 0) ? Math.sqrt(e) : 0
    
    result
      
  end
end
