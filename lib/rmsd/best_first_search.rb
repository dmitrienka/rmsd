require 'pqueue'
require 'nmatrix'

class BestFirstOrder
  def initialize params = {allow_improper: false, return_rotation: false, return_order: false}
    @allow_improper        = params[:allow_improper]
    @return_rotation       = params[:return_rotation]
    @return_order          = params[:return_order]
    @pq = PQueue.new([]){|a,b| a.cost < b.cost }
    @steps = 0
  end

  attr_accessor :allow_improper, :return_rotation, :return_order, :n, :coords1, :coords2, :steps

  def run_search mol1, mol2
    @coords1 = mol1.coords
    @coords2 = mol2.coords
    @n = mol1.n

    @pq.push Node.new([], self)

    while true
      node = @pq.pop
      if (node.n == n)
        return {rmsd: node.cost / Math.sqrt(n), rotation: node.rotation, order: node.order }
      end

      @pq.concat node.next_gen
      
    end
        
    
  end
  
  class Node

    def initialize order, parent
      @order  = order
      @parent = parent
      @rest   = (0...parent.n).to_a - order
      @n      = order.size
      get_cost
    end
    
    attr_reader :parent, :order, :rest, :n, :cost, :rotation
  
    def get_cost
      if (n == 0)
        @cost = 0
        @rotation = nil
      else
        co1 = parent.coords1.slice(0...n, 0...3)
        co2 = parent.coords2.transpose.
              permute_columns(order + rest ,  :convention => :intuitive).
              slice(0...3 ,0...n).transpose
        result = co1.kabsch co2, allow_improper: parent.allow_improper, rotation: parent.return_rotation
        @cost = result[:rmsd] * Math.sqrt(n)
        @rotation = result[:rotation]
        parent.steps = parent.steps + 1
      end
    end

    def next_gen
      rest.map{|i| Node.new order + [i], parent }
    end
  end

  
end

