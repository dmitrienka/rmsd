class FixedOrder
  def initialize params = {allow_improper: false, return_rotation: false, return_order: false}
    @allow_improper        = params[:allow_improper]
    @return_rotation       = params[:return_rotation]
    @return_order          = params[:return_order]
  end

  attr_accessor :allow_improper, :return_rotation, :return_order
  
  def run_search mol1, mol2
    result = mol1.coords.kabsch(mol2.coords,
                                allow_improper: allow_improper,
                                rotation: return_rotation)
    rusult[:order] = (0..mol2.n).to_a if return_order
    result
  end
  
end


class BruteForceOrder
  def initialize params = {allow_improper: false, return_rotation: false, return_order: false}
    @allow_improper        = params[:allow_improper]
    @return_rotation       = params[:return_rotation]
    @return_order          = params[:return_order]
  end

  attr_accessor :allow_improper, :return_rotation, :return_order
  
  def run_search mol1, mol2
    perms = (0...mol1.n).to_a.permutation.to_a
    perms.map do |perm|
      result = mol1.coords.kabsch(mol2.coords,
                                  allow_improper: allow_improper,
                                  rotation: return_rotation,
                                  order: perm)
      result[:order] = perm if return_order
      result
    end.min {|a, b| a[:rmsd] <=> b[:rmsd] }
    
  end
  
end
